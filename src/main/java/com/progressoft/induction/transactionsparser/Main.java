package com.progressoft.induction.transactionsparser;

import java.io.File;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        TransactionParser parser = new CsvTransactionParser();
        List<Transaction> csvTransactions = parser.parse(new File("src/main/resources/transactions.csv"));
        for(int i=0;i<csvTransactions.size();i++){
            System.out.println(csvTransactions.get(i));
        }

    }
}

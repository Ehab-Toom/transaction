package com.progressoft.induction.transactionsparser;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CsvTransactionParser implements TransactionParser {

    private static final int DESCRIPTION_INDEX = 0;
    private static final int DIRECTION_INDEX = 1;
    private static final int AMOUNT_INDEX = 2;
    private static final int CURRENCY_INDEX = 3;


    @Override
    public List<Transaction> parse(File transactionsFile) {

        BufferedReader reader = null;
        List<Transaction> myTransactions = new ArrayList<>();
        try {
            reader = new BufferedReader(new FileReader("src/main/resources/transactions.csv"));

        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] transactionLine = line.split(",");
            Transaction transaction = new Transaction();
            transaction.setDescription(transactionLine[DESCRIPTION_INDEX]);
            transaction.setDirection(transactionLine[DIRECTION_INDEX]);
            transaction.setAmount(new BigDecimal(transactionLine[AMOUNT_INDEX]));
            transaction.setCurrency(transactionLine[CURRENCY_INDEX]);

            myTransactions.add(transaction);
        }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return myTransactions;
    }
}
